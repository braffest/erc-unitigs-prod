#!/bin/bash
bucket=logan-staging
jobqueue=IndexThePlanetJobQueue
#jobqueue=IndexThePlanetJobQueueDisques

echo "Are you sure those jobs are fresh?"
read -p "Will $(tput bold)start the 500k run$(tput sgr0) to s3://$(tput bold)$bucket$(tput sgr0). Nothing will be erased from that bucket  Proceed? (yes/no) " response

case "$response" in
    [yY][eE][sS]|[yY])
        # Continue with the rest of the script
        ;;
    *)
        echo "Exiting the script."
        exit 1
        ;;
esac


#echo "181223-500k"> set
echo "221223-500k-batch3"> set
bash process_array.sh $bucket $jobqueue
