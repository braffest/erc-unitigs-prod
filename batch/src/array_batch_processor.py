import sys
import os
import boto3
import simple_batch_processor
from pathlib import Path

s3_path = os.environ.get('S3_PATH')
array_index = int(os.environ.get('AWS_BATCH_JOB_ARRAY_INDEX', 0))
region = os.environ.get('Region')
threads = os.environ.get('Threads')
output_bucket = os.environ.get('OutputBucket')
force_build_unitigs = os.environ.get('ForceBuildUnitigs') == "True"
only_unitigs = os.environ.get('OnlyUnitigs') == "True"

print(f"s3_path {s3_path} array_index {array_index} threads {threads} output_bucket {output_bucket} force_build_unitigs {force_build_unitigs} only_unitigs {only_unitigs}")

# Download the file from S3
s3 = boto3.resource('s3')
bucket_name, file_path = s3_path.replace('s3://', '').split('/', 1)
bucket = s3.Bucket(bucket_name)
file_name = file_path.split('/')[-1]
if not os.path.exists(file_name+'.downloaded'):
    bucket.download_file(file_path, file_name)
    Path(file_name+'.downloaded').touch()

# Read the specific line from the file
with open(file_name, 'r') as file:
    lines = file.readlines()
    if array_index < len(lines):
        accessions = lines[array_index].strip()
        retval = 0
        for accession in accessions.split(','):
            retval |= simple_batch_processor.process_accession(accession, region, threads, output_bucket, force_build_unitigs, only_unitigs) 
        if retval > 0:
            sys.exit(retval)
    else:
        print(f"No job found at array index {array_index}.")
