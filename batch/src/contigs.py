from datetime import datetime
from utils import ddb_log, ddb_log3, get_memory_and_time, get_machine_architecture
import subprocess
import os
import re
import constants
import boto3

def contigs(accession, outputBucket, nb_threads):
    start_time = datetime.now()
    arch = get_machine_architecture()
    k_value = str(31)
    ret = constants.CONTIGS_FAILED
    max_disk = 0

    unitigs_file = accession+".unitigs.fa"
    output_filename = accession+".contigs.fa"
    if not os.path.exists(unitigs_file) or \
            os.path.getsize(unitigs_file) == 0:
        print(f"no need to run Minia, because no unitigs.", flush=True)
        os.system("touch "+output_filename) #useful for my later scripts that check if the contigs were created or not
        ret = 0
    else:
        # hack: minia wants an input, but it doesnt matter as long as it has one solid kmer
        os.system('echo -e ">1\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" > ' + accession)
        get_memory_and_time([
           "perl","-i","-pe","if (/^>/) { s/^>"+accession+"_/>/; s/ka:/km:/; }",
           unitigs_file])
        max_attempts = 2
        for attempt in range(1, max_attempts + 1):
            try:
                runtime, mem, percent_cpu, max_disk, max_wait, stderr, stdout = get_memory_and_time(["/minia/build/bin/minia", 
                               "-in",accession,"-nb-cores",nb_threads,"-max-memory",str(1),
                               "-skip-bcalm","-skip-bglue","-redo-links"])
                ddb_log3(accession,'minia_'+arch+'_mem',mem,
                                   'minia_'+arch+'_time',runtime,
                                   'minia_'+arch+'_percent_cpu',percent_cpu)
                print(f"Minia succeeded.")
                ret = 0
                break
            except subprocess.CalledProcessError as e:
                print(f"Minia failed on attempt {attempt}.")
                # just make sure it's not one of those empty datasets
                if re.search(r'EXCEPTION:\serror\sopening\sfile:', e.stderr):
                    print("Ah, but it's a case when no contigs are output.")
                    os.system("touch "+output_filename) #useful for my later scripts that check if the contigs were created or not
                    ret = 0
                    break
                else:
                    print(f"Command: {e.cmd}")
                    print(f"Return Code: {e.returncode}")
                    print("----")
                    print(f"Stdout: {e.stdout}")
                    print("----")
                    print(f"Stderr: {e.stderr}")
                    print("----")
                if re.search(r'Command terminated by signal 9', e.stderr):
                    print("This is a memory problem, hopeless to retry")
                    break
                if attempt < max_attempts: print("Retrying...")

    contigs_time = datetime.now() - start_time

    return ret, max_disk


