import os
import boto3
from utils import ddb_log, ddb_log3, get_memory_and_time
from datetime import datetime
import constants
from concurrent.futures import ThreadPoolExecutor, as_completed
from functools import partial


def get_reads(accession, s3, outputBucket):
    print("downloading reads from S3 directly in SRA format..",flush=True)
    startTime = datetime.now()
    
    ret = 0
    file_size = 0
    local_file = None
    
    s3_path= f"s3://sra-pub-run-odp/sra/{accession}/{accession}"

    try:
        s3 = boto3.resource('s3')
        bucket_name, file_path = s3_path.replace('s3://', '').split('/', 1)
        bucket = s3.Bucket(bucket_name)
        local_file = file_path.split('/')[-1]+".sra"
        bucket.download_file(file_path, local_file)
        file_size = str(os.stat(local_file).st_size)
    except Exception as e:
        print("SRA download error:",str(e),'accession',accession,'s3_path',s3_path,flush=True)
        # disabling 
        if "An error occurred (404)" in str(e):
            """
            try:
                print("Accession not found using the regular S3 url. Retrying using prefetch..")
                os.system('vdb-config --prefetch-to-cwd')
                os.system('vdb-config --report-cloud-identity yes')
                os.system('vdb-config --simplified-quality-scores yes')
                local_file = accession + ".sra"
                pret = os.system('prefetch --max-size 500G ' + accession + ' -o ' + local_file)
                file_size = str(os.stat(local_file).st_size)
                if pret > 0:
                    print("SRA download failed using prefetch")
                    ret = constants.PREFETCH_FAILED 
            except:
                if "An error occurred (404)" in str(e):
                    print("Accession not found either prefetch either, giving up.")
                else:
                    print("SRA download exception using prefetch:",str(e),'accession',accession,'s3_path',s3_path)
                ret = constants.PREFETCH_FAILED 
            """
            # deciding to allow 404's and return gracefully. We won't bother if it's not on S3, don't wanna risk DDOSing NCBI.
            print("404 detected, stopping.",flush=True)
            return 0, 0
        else:
            ret = constants.PREFETCH_FAILED

    endTime = datetime.now()
    diffTime = endTime - startTime
    if ret == 0:
        ddb_log3(accession,'reads_sra_size',file_size,
                           'reads_sra_time',str(diffTime.seconds),
                           'reads_sra_date',str(datetime.now()))
    print(accession, "SRA reads S3 download time: " + str(diffTime.seconds) + " seconds",flush=True) 
            
    if ret > 0:
        return ret, 0

    print("checking for pesky SRA aligned format..",flush=True)
    startTime = datetime.now()
    runtime, mem, percent_cpu, max_disk, max_wait, stderr, stdout = get_memory_and_time([
                        "align-info",
                        accession+'.sra'])
 

    s3 = boto3.resource('s3')
    bucket_name = 'logan-refseq-mirror-brice'
    bucket = s3.Bucket(bucket_name)
    import pathlib
    pathlib.Path("/serratus-data/refseq/").mkdir(exist_ok=True)
    downloaded_size = 0
    files_to_download = []
    session = boto3.Session()
    client = session.client("s3")

    for line in stdout.split('\n'):
        ls = line.split(',')
        if ls[-1] != 'remote': continue
        name = ls[0]
        try:
            refseq_remote_file = name
            refseq_local_file =  '/serratus-data/refseq/' + name
            if not os.path.exists(refseq_local_file):
                print("Adding refseq file:", name, flush=True)
                client.head_object(Bucket=bucket_name, Key=refseq_remote_file)
                files_to_download.append(refseq_remote_file,)
            else:
                print(name,"already exists",flush=True)
        except Exception as e:
            print("Refseq file listing error:",str(e),'accession',accession,'ref name',name,flush=True)
            ddb_log(accession,'reads_sra_refseq_failed',name)
            #ret = constants.PREFETCH_FAILED
            print("Couldn't list  one of the remote dependencies, deleting .sra file and stopping.",flush=True)
            os.remove(accession+'.sra')
            return 0, 0

    func = partial(download_file, bucket_name, client)
    failed_downloads = []

    with ThreadPoolExecutor(max_workers=5) as executor:
        futures = {
            executor.submit(func, file_to_download): file_to_download for file_to_download in files_to_download
        }
        for future in as_completed(futures):
            if future.exception():
               failed_downloads.append((futures[future],future.exception()))
            else:
               downloaded_size += future.result()

    if len(failed_downloads)>0:
        for file,e in failed_downloads:
             print("Can't download refseq file ",file,flush=True)
             print(f"exeception : {e}", flush=True)

        print("Couldn't download one of the remote dependencies, deleting .sra file and stopping.",flush=True)
        print(f'Refseq mirror download error: {accession} {name}', flush=True)
        ddb_log(accession,'reads_sra_refseq_failed',name)
        os.remove(accession+'.sra') 
        return 0, 0

    endTime = datetime.now()
    diffTime = endTime - startTime
    print(accession, "SRA reads aligned handling time: " + str(diffTime.seconds) + " seconds, total downloaded:", downloaded_size/1024/1024,"MB",flush=True) 
    return ret, 0


def download_file(bucket_name, client, name):
    print("Downloading putative refseq:", name, flush=True)
    refseq_remote_file = name
    refseq_local_file =  '/serratus-data/refseq/' + name
    client.download_file(bucket_name, Key=refseq_remote_file, Filename=refseq_local_file)
    file_size = os.path.getsize(refseq_local_file)
    print("Downloaded refseq:", name, "file size:", file_size/1024/1024,"MB", flush=True)
    return file_size

