const cdk = require('aws-cdk-lib');
const iam = require('aws-cdk-lib/aws-iam');
const ec2 = require('aws-cdk-lib/aws-ec2');
const batch = require('aws-cdk-lib/aws-batch');


function generateShuffledSubarrays() {
  // Step 1: Create an array of 96 elements, with each number from 0 to 23 repeated four times
  const array = [];
  for (let i = 0; i < 24; i++) {
    for (let j = 0; j < 4; j++) {
      array.push(i);
    }
  }

  // Step 2: Shuffle the array to randomize the order
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]]; // Swap elements
  }

  // Step 3: Chunk the shuffled array into 24 subarrays of 4 elements each
  const subarrays = [];
  for (let i = 0; i < array.length; i += 4) {
    subarrays.push(array.slice(i, i + 4));
  }

  return subarrays;
}

const shuffledSubarrays = generateShuffledSubarrays();

function controlledShuffle(inputArray, callIndex) {
  // Select indices for this call
  const selectedIndices = shuffledSubarrays[callIndex];

  // Map selected indices to elements from the input array
  return selectedIndices.map(index => inputArray[index]);
}


var STACK_NAME = 'Pasteur';
var NB_CE = 4;

class MigrationStack extends cdk.Stack {
  /**
   *
   * @param {Construct} scope
   * @param {string} id
   * @param {StackProps=} props
   */
  constructor(scope, id, props) {
    super(scope, id, props);

    const nb_of_ce = parseInt(this.node.tryGetContext('nb_of_ce') || NB_CE);
    const nb_bjq = nb_of_ce;

    // EC2 Instance Types Parameter
    const instanceTypesParameter = new cdk.CfnParameter(this, 'InstanceTypes', {
      description: 'EC2 instance types',
      type: 'List<String>',
      default: 'c6g.4xlarge,c6g.8xlarge,c6g.12xlarge,c6g.16xlarge,c6g.metal,c6gn.4xlarge,c6gn.8xlarge,c6gn.12xlarge,c6gn.16xlarge,c7g.4xlarge,c7g.8xlarge,c7g.12xlarge,c7g.16xlarge,c7g.metal,c7gn.4xlarge,c7gn.8xlarge,c7gn.12xlarge,c7gn.16xlarge',
    });

    // Minimum vCPUs Parameter
    const minvcpusParameter = new cdk.CfnParameter(this, 'Minvcpus', {
      description: 'Minimum number of vCPUs per Compute Environment',
      type: 'Number',
      default: 0,
    });

    // Maximum vCPUs Parameter
    const maxvcpusParameter = new cdk.CfnParameter(this, 'Maxvcpus', {
      description: 'Maximum number of vCPUs per Compute Environment',
      type: 'Number',
      default: 300000,
    });

    // EBS Boot Size Parameter
    const ebsBootSizeParameter = new cdk.CfnParameter(this, 'EBSBootSize', {
      description: 'Size in GiB of EBS root volume',
      type: 'Number',
      default: 30,
    });

    // Allocation Strategy Parameter
    const allocationStrategyParameter = new cdk.CfnParameter(this, 'AllocationStrategy', {
      description: 'AllocationStrategy',
      type: 'String',
      default: 'SPOT_PRICE_CAPACITY_OPTIMIZED',
    });

    const role = new iam.Role(this, STACK_NAME + 'BatchInstanceRole', {
      assumedBy: new iam.ServicePrincipal('ec2.amazonaws.com'),
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AmazonEC2ContainerServiceforEC2Role'),
        iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonS3FullAccess'),
        iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonElasticMapReduceFullAccess'),
        iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonSSMManagedInstanceCore'),
        iam.ManagedPolicy.fromAwsManagedPolicyName('CloudWatchAgentServerPolicy')
      ]
    });

    // Create IAM policy
    const batchInstancePolicyG = new iam.Policy(this, 'BatchInstancePolicyG', {
      policyName: `BatchInstancePolicyGG-${this.stackName}`,
      statements: [
        new iam.PolicyStatement({
          effect: iam.Effect.ALLOW,
          actions: [
            'ec2:CreateVolume',
            'ec2:AttachVolume',
            'ec2:ModifyVolume',
            'ec2:DescribeVolumes',
            'ec2:DescribeVolumesModifications',
            'ec2:ModifyInstanceAttribute',
            'logs:CreateLogGroup',
            'dynamodb:UpdateItem',
          ],
          resources: ['*'],
        }),
      ],
    });

    // Attach policy to role
    batchInstancePolicyG.attachToRole(role);

    const batchInstanceProfileG = new iam.CfnInstanceProfile(this, 'BatchInstanceProfileG', {
      roles: [role.roleName],
    });


    // Create IAM role for SpotIamFleetRoleG
    const spotIamFleetRoleG = new iam.Role(this, 'SpotIamFleetRoleG', {
      assumedBy: new iam.CompositePrincipal(
        new iam.ServicePrincipal('spot.amazonaws.com'),
        new iam.ServicePrincipal('spotfleet.amazonaws.com'),
      ),
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AmazonEC2SpotFleetTaggingRole'),
      ],
    });

    // EC2 Block Device Mappings
    const blockDeviceMappings = [
      {
        deviceName: '/dev/xvda',
        ebs: {
          deleteOnTermination: true,
          encrypted: false,
          iops: 3000,
          volumeSize: cdk.Fn.ref('EBSBootSize'),
          volumeType: 'gp3',
        },
      },
      {
        deviceName: '/dev/xvdb',
        ebs: {
          deleteOnTermination: true,
          encrypted: false,
          volumeSize: 50,
          volumeType: 'gp3',
        },
      },
      {
        deviceName: '/dev/xvdc',
        ebs: {
          deleteOnTermination: true,
          encrypted: false,
          volumeSize: 50,
          volumeType: 'gp3',
        },
      },
      {
        deviceName: '/dev/xvdd',
        ebs: {
          deleteOnTermination: true,
          encrypted: false,
          volumeSize: 50,
          volumeType: 'gp3',
        },
      },
      {
        deviceName: '/dev/xvde',
        ebs: {
          deleteOnTermination: true,
          encrypted: false,
          volumeSize: 50,
          volumeType: 'gp3',
        },
      },
    ];

    // EC2 Launch Template UserData
    const userData = ec2.UserData.forLinux();
    userData.addCommands(
      `MIME-Version: 1.0
Content-Type: multipart/mixed; boundary=="==MYBOUNDARY=="

--==MYBOUNDARY==
Content-Type: text/x-shellscript; charset="us-ascii"

#!/bin/bash

exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

# taken care of by graviton AMI
# https://stackoverflow.com/questions/41073906/how-to-attach-and-mount-volumes-to-an-ec2-instance-using-cloudformation
#curl "https://awscli.amazonaws.com/awscli-exe-linux-aarch64.zip" -o "awscliv2.zip"
#yum install -y unzip 
#unzip awscliv2.zip
#sudo ./aws/install
#. ~/.bash_profile

aws configure set default.region "us-east-1"
aws configure get default.region

TOKEN=\`curl -s -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"\`
INSTANCE_TYPE=\$(curl -H "X-aws-ec2-metadata-token: \$TOKEN" -s http://169.254.169.254/latest/meta-data/instance-type)

if [ "\$INSTANCE_TYPE" != "c6g.4xlarge" ] && [ "\$INSTANCE_TYPE" != "c7g.4xlarge" ] && [ "\$INSTANCE_TYPE" != "c6gn.4xlarge" ] && [ "\$INSTANCE_TYPE" != "c7gn.4xlarge" ]; then
  echo "Instance type is not 4xlarge"

  # Get vCPUs
  NB_VCPUS=\`nproc\`
  GB_PER_VCPUS=3  # jobs using 4 cores, i.e. sub-20Gbp accessions, will have 4*3*4 = 50 GB 
                  # jobs using 8 cores, i.e. sub-60Gbp accessions, will have 4*3*8 = 100 GB
                  # the 4 is because I'll have a raid0 of 4 disks 
                  # so, 50 GB per 4-core job makes 200 GB for 4xlarge, and 1200 GB for metal. If we were not to resize, allocate 1200/4 per EBS = 300
  # Compute new EBS Size
  SIZE=$(( \$NB_VCPUS * \$GB_PER_VCPUS ))

  # remember volume initial sizes before resizing
  # Function to get the current size of a volume by its device name
  get_volume_size() {
    lsblk | grep "\$1" | awk '{print $4}' # Simple version
  }

  for volume in nvme1n1 nvme2n1 nvme3n1 nvme4n1; do
    size_var="initial_size_\$volume"
    eval "\$size_var=\$(get_volume_size "\$volume")"
    eval "echo Stored initial size for volume \$volume: \$$size_var"
  done 

  # Get all volume IDs associated with the instance, excluding the root device /dev/xvda
  VOLUME_IDS="\$(/sbin/ebsnvme-id /dev/xvdb -v | awk '{print $3}') \$(/sbin/ebsnvme-id /dev/xvdc -v | awk '{print $3}') \$(/sbin/ebsnvme-id /dev/xvdd -v | awk '{print $3}') \$(/sbin/ebsnvme-id /dev/xvde -v | awk '{print $3}')"
  echo "Volume IDs: \$VOLUME_IDS"

  # Loop through each volume ID and perform resizing (excluding /dev/xvda)
  for VOLUME_ID in \$VOLUME_IDS; do
    DELAY=5
    MAX_RETRIES=10
    TRY_COUNT=0

    while :; do
      # Attempt to resize the EBS volume.
      if aws ec2 modify-volume --volume-id \$VOLUME_ID --size \$SIZE; then
        echo "Resized volume \$VOLUME_ID"
        break
      else
        echo "Failed to resize volume \$VOLUME_ID, retrying in \$DELAY seconds..."
        sleep \$DELAY
        DELAY=$((DELAY + 5))
        TRY_COUNT=$((TRY_COUNT + 1))
        if [ "\$TRY_COUNT" -ge "\$MAX_RETRIES" ]; then
          echo "Maximum retries reached for resizing volume \$VOLUME_ID"
          exit 1
        fi
      fi
    done
  done

  # Continually check the size of each volume until it changes
  for volume in nvme1n1 nvme2n1 nvme3n1 nvme4n1; do
    size_var="initial_size_\$volume"
    eval "initial_size=\$$size_var"
    echo "Checking for size change in volume \$volume..."

    while :; do
      current_size=\$(get_volume_size "\$volume")

      # Check if the size has changed
      if [[ "\$current_size" != "\$initial_size" ]]; then
        echo "The size of \$volume has changed from \$initial_size to \$current_size"
        break
      else
        echo "Waiting for size change in \$volume..."
        sleep 5 # Wait for a few seconds before re-checking
      fi
    done
  done              

else # if c6g.4xlarge or c7g.4xlarge
  echo "Instance type is 4xlarge, doing nothing more"
fi

# make raid
MOUNT_POINT="/serratus-data"
mkdir -p \$MOUNT_POINT

# taken care of by graviton AMI
#yum install -y mdadm

mdadm --create --verbose /dev/md0 --level=0 --raid-devices=4 /dev/xvdb /dev/xvdc /dev/xvdd /dev/xvde
mkfs.ext4 /dev/md0
mount /dev/md0 \$MOUNT_POINT

# brice's trick
sysctl vm.dirty_writeback_centisecs=200
sysctl vm.dirty_bytes=4294967296
sysctl vm.dirty_background_bytes=2147483648

lsblk > /lsblk.txt
df -Th > /dfT.txt

--==MYBOUNDARY==--`
    );

    const launchTemplate = new ec2.CfnLaunchTemplate(this, 'BatchLaunchTemplateGraviton', {
      launchTemplateName: 'BatchLaunchTemplateGraviton',
      versionDescription: 'Initial version',
      launchTemplateData: {
        blockDeviceMappings,
        userData: cdk.Fn.base64(userData.render()),
      },
    });

    // Batch Security Group
    const securityGroupId = cdk.Fn.importValue('Logan-LargeScaleVPC-SecurityGroup');


    var list_of_ce_4_bjq = [];
    var list_of_ce = [];


    for (var ce = 0; ce < nb_of_ce; ce++) {
      // Batch Compute Environment
      const batchComputeEnvironment = new batch.CfnComputeEnvironment(this, 'BatchECSCEG' + ce, {
        type: 'MANAGED',
        computeResources: {
          allocationStrategy: allocationStrategyParameter,
          launchTemplate: {
            launchTemplateId: launchTemplate.ref,
            version: launchTemplate.attrLatestVersionNumber,
          },
          minvCpus: cdk.Fn.ref('Minvcpus'),
          maxvCpus: cdk.Fn.ref('Maxvcpus'),
          type: 'SPOT',
          instanceRole: batchInstanceProfileG.ref,
          instanceTypes: cdk.Fn.ref('InstanceTypes'),
          ec2KeyPair: 'serratus',
          tags: {
            Name: 'IndexThePlanet-Unitigs-SPOTG-'+ce,
          },
          imageId: 'ami-0a7bf24641a8caae5', // Replace with your custom AMI ID
          spotIamFleetRole: spotIamFleetRoleG.roleArn,
          subnets: cdk.Fn.split(',', cdk.Fn.importValue('Logan-LargeScaleVPC-PrivateSubnets')),
          securityGroupIds: [securityGroupId],
        },
        replaceComputeEnvironment: false,
        state: 'ENABLED',
      });

      list_of_ce_4_bjq.push(
         {
          computeEnvironment : batchComputeEnvironment.ref,
          order : 1
        }
      )

      list_of_ce.push(batchComputeEnvironment);

    }

    for (var bjq = 0; bjq < nb_bjq; bjq++) {
      const batchJobQueueGraviton = new batch.CfnJobQueue(this, 'BatchJobQueueGraviton' + bjq, {
        jobQueueName: 'IndexThePlanetJobQueueGraviton' + bjq,
        computeEnvironmentOrder: [...controlledShuffle(list_of_ce_4_bjq, bjq)],
        priority: bjq+1,
        state: 'ENABLED',
      });
    }

    let hash_of_jobdef = [
      {
        name: 'logan-8gb-jobg',
        mem: 7000,
        vcpu: 4,
      },
      {
        name: 'logan-15gb-jobg',
        mem: 14500,
        vpcu: 8
      },
      {
        name: 'logan-31gb-jobg',
        mem: 29000,
        vpcu: 16
      },
      {
        name: 'logan-63gb-jobg',
        mem: 58000,
        vcpu: 32
      }
    ]

    for(var jd = 0; jd < hash_of_jobdef.length; jd++){

      const batchEightGigsJobDefinitionG = new batch.CfnJobDefinition(this, 'BatchEightGigsJobDefinition'+jd, {
        type: 'container',
        jobDefinitionName: hash_of_jobdef[jd].name,
        containerProperties: {
          image: cdk.Fn.join('', [
            cdk.Fn.ref('AWS::AccountId'),
            '.dkr.ecr.',
            cdk.Fn.ref('AWS::Region'),
            '.amazonaws.com/logan-batch-unitigs-job-aarch64:latest',
          ]),
          resourceRequirements: [
            { type: 'MEMORY', value: ''+hash_of_jobdef[jd].mem }, // Vcpu/mem ratio of C5's
            { type: 'VCPU', value: ''+hash_of_jobdef[jd].vcpu },
          ],
          mountPoints: [
            {
              containerPath: '/serratus-data',
              readOnly: false,
              sourceVolume: 'serratus-data',
            },
          ],
          volumes: [
            {
              name: 'serratus-data',
              host: {
                sourcePath: '/serratus-data',
              },
            },
          ],
        },
        retryStrategy: {
          attempts: 5,
          evaluateOnExit: [
            {
              onStatusReason: 'Host EC2*',
              action: 'RETRY',
            },
            {
              onReason: '*',
              action: 'EXIT',
            },
          ],
        },
      });
    }

    for (var ce = 0; ce < list_of_ce.length; ce++){
      // create an Output
      const compenv = new cdk.CfnOutput(this, 'ComputeEnvironment' + ce, {
        value: list_of_ce[ce].ref,
        description: 'Compute Environment #' + ce,
      });
    }
  }
}


module.exports = { MigrationStack }
