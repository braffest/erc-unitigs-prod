This is Greg's automated script to generate our Graviton template

To install:

sudo yum install npm
npm install aws-cdk-lib
npm install -g aws-cdk

To run:
./generate_template.sh > templateg.yaml
