bucket=logan-dec2023-testbucket
date=$(date +"%b%d-%Y")
arch=$(uname -m)
tag=cuttlefisha-$arch-$date

filename=subset_yeasts_artem

# run those steps manually to see what's up

#bash post_1_find_non_unitigged.sh

#bash post_2_filter_removed_accessions.sh

cd batch
# ..and this line
for accession in `cat ../post_2_filter_removed_accessions.txt`
# alternative: do all the subset regardless of whether it was done already or not
do
    # get its size
    size=$(grep $accession ../subsets/$filename.tsv | awk '{print $2}')
    echo $accession $size
    python simple_submit_job.py $accession $size $tag $bucket
done
