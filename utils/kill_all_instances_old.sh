#!/bin/bash

HOW_MANY_AT_A_TIME=1000

# Define the prefix for instance names
instance_name_prefix="IndexThePlanet-Unitigs-SPOTG4"

instance_ids=$(aws ec2 describe-instances \
    --filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values=${instance_name_prefix}*" \
    --query 'Reservations[*].Instances[*].InstanceId' \
    --output text)

# Check if there are matching instances
if [ -z "$instance_ids" ]; then
    echo "No running instances with the specified name prefix."
    exit 0
fi

# Convert string to an array
names_array=($instance_ids)

# Total number of names
total_names=${#names_array[@]}

# Loop through names in groups of $HOW_MANY_AT_A_TIME
for ((i=0; i<total_names; i+=$HOW_MANY_AT_A_TIME)); do
    # Extract 10 or remaining names
    group=(${names_array[@]:i:$HOW_MANY_AT_A_TIME})
    
    # Join array elements into a space-separated string
    group_string="${group[*]}"
    
    # Do something with each group of $HOW_MANY_AT_A_TIME names
    echo "Processing group: $group_string"
    # Terminate the filtered instances
    aws ec2 terminate-instances --instance-ids $group_string

done


echo "Instances terminated."
