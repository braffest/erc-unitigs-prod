#!/bin/bash

# Directory containing .sra files
DIRECTORY="$HOME/scratch/"

# Iterate over each item in the scratch directory
for dir in "$DIRECTORY"*/ ; do
    # Check if the directory name matches the pattern "logan-xxxx"
    if [[ $(basename "$dir") == logan-* ]]; then
        # Extract the "xxxx" part and output it
        logandir=$(basename "$dir" | sed 's/logan-//')
        # Submit a job for this file
        sbatch  -o $PWD/slurm-logs/slurm-%j.out -e $PWD/slurm-logs/slurm-%j.err -q seqbio -p seqbio --mem 250G -c 10 $PWD/run_local_contigs.sh "$logandir"
	fi
done 

