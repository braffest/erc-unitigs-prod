import boto3

# Initialize a session using Boto3
session = boto3.Session()
dynamodb = session.resource('dynamodb')

# Specify your table name
table_name = 'Logan-BatchOpDashboard-BatchJobData-1202518NCTDVH'

# Get the DynamoDB table
table = dynamodb.Table(table_name)

# Scan the table for items where 'startedAt' does not exist
response = table.scan(
    FilterExpression='attribute_not_exists(startedAt)'
)

items = response.get('Items', [])

# Iterate over the items and delete them
for item in items:
    primary_key = {'jobId': item['jobId']}
    table.delete_item(Key=primary_key)

print(f"Deleted {len(items)} items from '{table_name}' where 'startedAt' does not exist.")

