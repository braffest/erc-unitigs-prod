# sets maxvcpus for compute environments

import boto3
import sys

def aws_batch_client():
    """Initialize and return an AWS Batch client."""
    return boto3.client('batch')

def list_compute_environments(client):
    """List all compute environments."""
    response = client.describe_compute_environments()
    return [env['computeEnvironmentName'] for env in response['computeEnvironments']]

def update_compute_environment(client, compute_env_name, maxvcpus):
    """Disable a specified compute environment."""
    client.update_compute_environment(computeEnvironment=compute_env_name, computeResources={'maxvCpus':maxvcpus})

def main():
    client = aws_batch_client()
    compute_envs = list_compute_environments(client)
    print("Available Compute Environments:", compute_envs)
    maxvcpus=int(sys.argv[1])

    for compute_env_name in compute_envs:
        if not compute_env_name.startswith('BatchECSCE'): continue
        try:
            update_compute_environment(client, compute_env_name, maxvcpus)
            print(f"Compute environment '{compute_env_name}' has been updated to {maxvcpus} max vCPUs.")
        except:
            print(f"problem with that one, skipping {compute_env_name}")

if __name__ == "__main__":
    main()
