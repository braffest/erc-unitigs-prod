#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

set=$SCRIPT_DIR/../sets/$(cat $SCRIPT_DIR/../set).tsv
if [[ -f "$set" ]]; then
    line_count=$(wc -l < "$set")
    random_line=$((RANDOM % line_count + 1))
    accession_size=$(sed -n "${random_line}p" "$set")
    accession=$(echo $accession_size | awk '{print $1}')
    size=$(echo $accession_size | awk '{print $2}')
else
    echo "Error: File $set not found."
fi

#accession=SRR11505003
#size=11

echo "Testing $accession (size $size mbases).."

aws sts get-session-token --duration-seconds 3000 > credentials.json
echo "AWS_ACCESS_KEY_ID=$(jq -r '.Credentials.AccessKeyId' credentials.json)" > credentials.env
echo "AWS_SECRET_ACCESS_KEY=$(jq -r '.Credentials.SecretAccessKey' credentials.json)" >> credentials.env
echo "AWS_SESSION_TOKEN=$(jq -r '.Credentials.SessionToken' credentials.json)" >> credentials.env

NAME=batch-unitigs-test-local-job
memorylimit="--memory=8g"
mv credentials.env $SCRIPT_DIR/../batch/src/
bash -c "cd $SCRIPT_DIR/../batch/src/ && 
	docker build --quiet -t $NAME \
	    --build-arg AWS_DEFAULT_REGION=us-east-1 \
	    . >/dev/null &&\
    docker run \
    --ulimit nofile=65535:65535 \
    --env-file credentials.env \
    -e AWS_DEFAULT_REGION=us-east-1\
    -e Accessions=$accession\
    -e ForceBuildUnitigs=True\
    -e OutputBucket=logan-dec2023-testbucket\
    $memorylimit \
    $NAME \
    python simple_batch_processor.py >/dev/null && rm -f credentials.env"

rm -f credentials.json 

echo "Comparing.."
aws s3 cp s3://logan-staging/c/$accession.contigs.fa.zst .
aws s3 cp s3://logan-staging/u/$accession.unitigs.fa.zst .

if [[ -f "$accession.unitigs.fa.zst" ]]; then
	mv $accession.unitigs.fa.zst prod_$accession.unitigs.fa.zst

	aws s3 cp s3://logan-dec2023-testbucket/u/$accession.unitigs.fa.zst .
	mv $accession.unitigs.fa.zst test_$accession.unitigs.fa.zst

	zstd -d prod_$accession.unitigs.fa.zst
	zstd -d test_$accession.unitigs.fa.zst

	echo "unitigs:"
	seqkit stats prod_$accession.unitigs.fa 
	seqkit stats test_$accession.unitigs.fa

	rm -f prod_$accession.unitigs.fa* test_$accession.unitigs.fa*
fi

if [[ -f "$accession.contigs.fa.zst" ]]; then
	mv $accession.contigs.fa.zst prod_$accession.contigs.fa.zst

	aws s3 cp s3://logan-dec2023-testbucket/c/$accession.contigs.fa.zst .
	mv $accession.contigs.fa.zst test_$accession.contigs.fa.zst

	zstd -d prod_$accession.contigs.fa.zst
	zstd -d test_$accession.contigs.fa.zst

	echo "contigs:"
	seqkit stats prod_$accession.contigs.fa 
	seqkit stats test_$accession.contigs.fa

	rm -f prod_$accession.contigs.fa* test_$accession.contigs.fa*
fi
