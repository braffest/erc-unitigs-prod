#!/bin/bash

# uncomment this to specialie script to a single job array 
#jobid=dc7c3280-7c80-473a-be96-233c8612e392
jobqueue=IndexThePlanetJobQueue

# Function to describe a job and print its duration
describe_job_and_print_duration() {
    local_job_id=$1
    job_description=$(aws batch describe-jobs --jobs $local_job_id)

    # Extract start and end times
    started_at=$(echo $job_description | jq '.jobs[0].startedAt')
    stopped_at=$(echo $job_description | jq '.jobs[0].stoppedAt')

    # Calculate duration
    if [ "$started_at" != "null" ] && [ "$stopped_at" != "null" ]; then
        duration=$((($stopped_at - $started_at) / 1000)) # Convert from milliseconds to seconds
        echo "Job ID: $local_job_id, Duration: $duration seconds"
    else
        echo "Job ID: $local_job_id, Duration: Not available"
    fi
}

analyze_job_array() {
	job_id=$1
	echo "Job array found for job ID $job_id"
        job_description=$(aws batch describe-jobs --jobs $job_id)
        array_properties=$(echo "$job_description" | jq '.jobs[0].arrayProperties')

	array_size=$(echo "$array_properties" | jq -r '.size')

	s3_path=$(echo "$job_description" |  jq -r '.jobs[0].container.environment[] | select(.name == "S3_PATH").value')
	echo "Array S3 path: $s3_path"

	for (( counter=0; counter<array_size; counter++ )); do
		child_job_id="${job_id}:${counter}"
		describe_job_and_print_duration $child_job_id
	done
}

# Function to process jobs based on their status
process_jobs() {
    status=$1
    job_ids=$(aws batch list-jobs --job-queue $jobqueue --job-status $status --query 'jobSummaryList[*].jobId' --output json | jq -r '.[]')

    for job_id in $job_ids; do
        job_description=$(aws batch describe-jobs --jobs $job_id)
        array_properties=$(echo "$job_description" | jq '.jobs[0].arrayProperties')

	if [ "$array_properties" != "null" ]; then
		analyze_job_array $job_id
	else
		# Print the duration for individual jobs
		describe_job_and_print_duration $job_id >/dev/null
	fi
    done
}

# Check if jobid is empty
if [ -z "$jobid" ]; then 
    # Process succeeded and failed jobs
    process_jobs "SUCCEEDED"
    process_jobs "FAILED"
else
    # jobid is not empty, run second command
    analyze_job_array $jobid > duration_of_jobs.txt
fi

# Extract durations from the log and store them in an array
durations=($(grep -oP 'Duration: \K[0-9]+' duration_of_jobs.txt))

# Calculate the mean
total=0
for duration in "${durations[@]}"; do
    total=$((total + duration))
done
mean=$((total / ${#durations[@]}))
echo "Mean Duration: $mean seconds"

# Calculate the median
sorted=($(printf '%s\n' "${durations[@]}" | sort -n))
count=${#sorted[@]}
if ((count % 2 == 0)); then
    mid=$((count / 2))
    median=$(((sorted[mid] + sorted[mid - 1]) / 2))
else
    median=${sorted[count / 2]}
fi
echo "Median Duration: $median seconds"


# Calculate the 10th percentile
percentile_index=$((count / 10))
tenth_percentile=${sorted[percentile_index]}
echo "10th Percentile Duration: $tenth_percentile seconds"
