d_accession_type = dict()
d_accession_size = dict()

for line in open("cloudwatch_batch_logs_analysis.accessions.run5.acc.tsv"):
    acc, size = line.split()
    size = int(size)
    d_accession_size[acc] = size
    if size < 20000:
        d_accession_type[acc] = '4vcpus'
    elif size >= 20000 and size < 60000: 
        d_accession_type[acc] = '8vcpus'
    else:
        print("huh, big accession shouldn't have been submitted:",acc)

accessions_in_complete_time_but_not_accessions = 0
for line in open("cloudwatch_batch_logs_analysis.complete_time.acc.run5.tsv"):
    acc, size = line.split()
    size = int(size)
    if acc not in d_accession_size:
        d_accession_size[acc] = size
        accessions_in_complete_time_but_not_accessions += 1
        if size < 20000:
            d_accession_type[acc] = '4vcpus'
        elif size >= 20000 and size < 60000: 
            d_accession_type[acc] = '8vcpus'

print("parsed tsvs")
print(f"{accessions_in_complete_time_but_not_accessions} accessions in complete_time.acc but not accessions.acc")

d_complete_times = dict()
overhead = 0
total_job_times = 0
total_job_times_4vcpus = 0
total_job_times_8vcpus = 0
s_4vcpus_accessions = set()
s_8vcpus_accessions = set()
n_unknown_accessions = 0

d_jobid = dict()
for line in open("cloudwatch_batch_logs_analysis.accessions.run5.txt"):
    ls = line.split()
    jobid, accession = ls[0], ls[-1]
    if jobid not in d_jobid:
        d_jobid[jobid] = [accession]
    else:
        d_jobid[jobid] += [accession]

d_jobid2 = dict()
for line in open("cloudwatch_batch_logs_analysis.complete_time.run5.txt"):
    ls = line.split()
    if ls[-2] == "k-mer":
        #that's cuttlefish
        continue
    elif (ls[-2] == "request" and ls[-1] == "rate.") or ls[-1] == "Unknown":
        #that's S3 rate limits
        continue
    elif not ls[-2].isdigit():
        print("unexpected line:",line)
        continue
    jobid, accession, time = ls[0], ls[2], int(ls[-2])
    if jobid not in d_jobid2:
        d_jobid2[jobid] = [accession]
    else:
        d_jobid2[jobid] += [accession]

    total_job_times += time
    if accession in d_complete_times:
        #print(f"accession {accession} processed another time, took {time} vs {d_complete_times[accession]}")
        overhead += min(time,d_complete_times[accession])
        d_complete_times[accession] = max(time,d_complete_times[accession])
    else:
        d_complete_times[accession] = time
    if accession not in d_accession_type:
        n_unknown_accessions += 1
    else:
        if d_accession_type[accession] == '4vcpus':
            total_job_times_4vcpus += time
            s_4vcpus_accessions.add(accession)
        else:
            total_job_times_8vcpus += time
            s_8vcpus_accessions.add(accession)

print(n_unknown_accessions,"unknown accessions for size")
print("total job times",total_job_times,"seconds")
print(len(s_4vcpus_accessions),"4vcpus jobs",total_job_times_4vcpus,"seconds")
print(len(s_8vcpus_accessions),"8vcpus jobs",total_job_times_8vcpus,"seconds")
print("wasted overhead",overhead,"seconds")

d_seqlength = {}
for line in open('cloudwatch_batch_logs_analysis.totalseqlength.run5.txt'):
    ls = line.split()
    jobid, seqlength = ls[0], int(ls[-1][:-1])
    if jobid not in d_seqlength:
        d_seqlength[jobid] = [seqlength]
    else:
        d_seqlength[jobid] += [seqlength]

missing_jobids = 0
missing_accessions = 0
size_smaller_than_seqlength =0
accessions_not_analyzed = 0
accessions_to_redo = 0
accessions_to_redo_size = 0
accessions_to_redo_zero = 0
accessions_to_redo_size_zero = 0
accessions_to_redo_stealth = 0
accessions_to_redo_size_stealth = 0
accessions_total = 0
accessions_total_size = 0

for jobid in d_seqlength:
    if jobid in d_jobid: 
        accessions = d_jobid[jobid]
        seqlengths = d_seqlength[jobid]
        if len(accessions) != len(seqlengths) and jobid in d_jobid2:
            accessions = d_jobid2[jobid]
    elif jobid in d_jobid2:
        accessions = d_jobid2[jobid]
        seqlengths = d_seqlength[jobid]
    else:
        missing_jobids += 1
        continue

    if len(accessions) != len(seqlengths):
        print("length mismatch",len(accessions),len(seqlengths))
        accessions_not_analyzed += len(accessions)
    else:
        for i in range(len(accessions)):
            acc = accessions[i]
            seqlength = seqlengths[i]
            if acc not in  d_accession_size:
                missing_accessions += 1
                continue
            size = d_accession_size[acc]
            seqlength /= 1000000
            seqlength = int(seqlength)
            accessions_total_size += size
            accessions_total += 1
            if size == 0:
                if seqlength != 0:
                    print("how?",acc,"true",size,"reported",seqlength)
                    accessions_to_redo += 1
                    accessions_to_redo_stealth += 1
            else:
                if abs(size-seqlength)/size > 0.01:
                    accessions_to_redo += 1
                    accessions_to_redo_size += size
                    if seqlength == 0:
                        accessions_to_redo_zero += 1
                        accessions_to_redo_size_zero += size
                    else:
                        accessions_to_redo_stealth += 1
                        accessions_to_redo_size_stealth += size
            if size < seqlength:
                size_smaller_than_seqlength += 1

print(f"{accessions_to_redo}/{accessions_total} accessions to redo, total size {accessions_to_redo_size}/{accessions_total_size} mbases")
print(f"breakdown:")
print(f" zero-bytes output: {accessions_to_redo_zero}/{accessions_to_redo}, total size {accessions_to_redo_size_zero}/{accessions_to_redo_size} mbases")
print(f" non-zero output: {accessions_to_redo_stealth}/{accessions_to_redo}, total size {accessions_to_redo_size_stealth}/{accessions_to_redo_size} mbases")
print(f"{size_smaller_than_seqlength} odd accessions with size < seqlength")
print(f"{missing_jobids} missing job ids in complete_time.run5.txt")
print(f"{missing_accessions} missing accessions in accessions.run5.txt")
print(f"{accessions_not_analyzed} accessions not analyzed")
