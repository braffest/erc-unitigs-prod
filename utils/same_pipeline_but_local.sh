#!/bin/bash
# aws s3 cp s3://sra-pub-run-odp/sra/$accession/$accession . && mv $accession $accession.sra

threads=10

set -e 

workdir=$HOME/scratch/logan-$(echo $RANDOM | md5sum | head -c 10)/

function cleanup {
  local line=${BASH_LINENO[0]}
  local command=${BASH_COMMAND}

  echo "run_local exited abruptly" > $workdir/error.txt
  echo "Error occurred on or near line $line: $command" >> $workdir/error.txt
}

trap cleanup EXIT

if [ $# -lt 1 ]; then
   echo 1>&2 "$0: not enough arguments"
   exit 1
fi
accession=$1
ulimit -n 2048

sradir=$HOME/scratch/logan_sra/
DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

mkdir  $workdir
cd $workdir

if [ ! -f "$accession.unitigs.fa" ]; then

    \time $HOME/tools/cuttlefish-rchikhi/build/src/cuttlefish build -m 100 \
             -s $sradir/$accession.sra -k 31 -t $threads --read \
             -o $accession.unitigs
else
    echo "unitigs already exist"
fi

echo -e ">1\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" > $accession
export LANG=C
#sed -i 's/ka:/km:/g' $accession.unitigs.fa
python $DIR/replace_string.py $accession.unitigs.fa "ka:" "km:"

\time $HOME/tools/minia/build/bin/minia -in $accession -nb-cores $threads -max-memory 2000 \
                               -skip-bcalm -skip-bglue -redo-links

sed -i "s/^>/>${accession}_/g" ${accession}.unitigs.fa 
sed -i "s/^>/>${accession}_/g" ${accession}.contigs.fa 

sed -i -e '/^>/s/km:/ka:/' -e '/^>/s/LN:i:[^ ]* //' -e '/^>/s/KC:i:[^ ]* //' ${accession}.contigs.fa
#sed -i 's/km:/ka:/g' ${accession}.unitigs.fa
python $DIR/replace_string.py $accession.unitigs.fa "km:" "ka:"

\time f2sz -l 13 -b 128M -F -i -f -T $threads $accession.unitigs.fa
\time f2sz -l 13 -b 128M -F -i -f -T $threads $accession.contigs.fa

mv $accession.unitigs.* $DIR/unitigs/
mv $accession.contigs.* $DIR/contigs/
rm -f $accession $accession.h5

rmdir $workdir

echo "all done for $accession! deleting $sradir/$accession.sra"
rm -f $sradir/$accession.sra
