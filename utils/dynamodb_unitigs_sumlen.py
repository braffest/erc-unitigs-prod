# this one parses the Logan table and get aggregates
# let's see how quick this is, and how painful in terms of costs
# well, it's not fast. 2 minutes for 2.7M entries

import boto3

# Initialize a DynamoDB client
dynamodb = boto3.resource('dynamodb')

table = dynamodb.Table('Logan')
# Scan the table
scan_kwargs = {
       'ProjectionExpression': 'accession, seqstats_unitigs_sumlen'
       }

# Structure to hold accession counts and instance types
g = open("dynamodb_unitigs_sumlen.txt","w")
while True:
    response = table.scan(**scan_kwargs)
    items = response['Items']


    for item in items:
        accession = item['accession']
        if 'seqstats_unitigs_sumlen' not in item: continue
        unitigs_sumlen = item['seqstats_unitigs_sumlen']
        g.write(f"{accession}\t{unitigs_sumlen}\n")

    # Check if there are more items to fetch
    if 'LastEvaluatedKey' in response:
        scan_kwargs['ExclusiveStartKey'] = response['LastEvaluatedKey']
    else:
        break  # Exit the loop if no more items
g.close()
