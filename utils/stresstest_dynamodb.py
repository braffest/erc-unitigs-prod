import boto3
import botocore
import decimal

"""
 atomic update of counters inside dynamodb
"""
def ddb_counter(
        counter_name, val,
        region='us-east-1'
    ):
    session = boto3.Session(region_name=region)
    dynamodb = session.resource('dynamodb')

    table = dynamodb.Table("Logan-Counters")

    try:
        # Put item into table
        response = table.update_item(
            Key={
                'date': 'dummydate' 
                },
            UpdateExpression=f"SET {counter_name} = if_not_exists({counter_name}, :start) + :val",
            ExpressionAttributeValues={
                ':val': decimal.Decimal(val),
                ':start': decimal.Decimal(0)
                },
        )

        # Check for successful response
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            return True
        else:
            print("DynamoDB log error:", response['ResponseMetadata']['HTTPStatusCode'])
            return False

    except botocore.exceptions.ClientError as e:
        print("DynamoDB counter update error:", e.response['Error']['Message'], counter_name, current_date)
        return False


region='us-east-1'
session = boto3.Session(region_name=region)
dynamodb = session.resource('dynamodb')

table = dynamodb.Table("Logan-Counters")

response = table.delete_item(
		Key={
			'date': 'dummydate' 
			})


import multiprocessing

def operation(process_number):
    ddb_counter('test',1)

num_processes = 300
num_jobs = 500000
with multiprocessing.Pool(processes=num_processes) as pool:
	# Map the dummy_operation function to each process
	pool.map(operation, range(num_jobs))

print(f"Done stresstesting")
