#!/bin/bash

# First, prepare the patterns to match the format in the CSV file
sed 's/^/"/; s/$/"/' post_1_find_non_unitigged.txt > search_patterns.txt

# Now, use zgrep to find these patterns in the CSV file.
# The -F flag treats the search patterns as fixed strings,
# and the -f flag points to the file with the list of patterns.
zstdgrep -F -f search_patterns.txt Athena_Dec_10_public.csv.zst | cut -d ',' -f 1 | tr -d '"' > post_2_filter_removed_accessions.txt

echo "$(wc -l post_2_filter_removed_accessions.txt|awk '{print $1}')/$(wc -l post_1_find_non_unitigged.txt|awk '{print $1}') public non-unitigged accessions remaining, see post_2_filter_removed_accessions.txt"

# Clean up the temporary search patterns file
rm search_patterns.txt
