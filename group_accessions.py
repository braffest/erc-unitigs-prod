import sys

def process_accessions(file_path, max_group_time=2000, max_group_elements=500):
    with open(file_path, 'r') as file:
        group = []  # Store current group of accessions
        group_time = 0  # Cumulative execution time of the current group
        group_size = 0  # Cumulative size of current group

        for line in file:
            columns = line.strip().split('\t')
            if len(columns) >= 2 and columns[0] != "Run" and columns[0] != "acc":
                accession, size = columns[0], int(columns[1])
                estimated_time = int(size / 10) + 1

                # Check if adding this accession would exceed time or element limits
                if group_time + estimated_time <= max_group_time and len(group) < max_group_elements:
                    group.append((size, accession))
                    group_time += estimated_time
                    group_size += size
                else:
                    # Output the current group if it's not empty
                    if group:
                        print(f"{','.join([x[1] for x in sorted(group)])}\t{int(group_size)}")
                    group = [(size,accession)]
                    group_time = estimated_time
                    group_size = size

        # Output the last group if not empty
        if group:
            print(f"{','.join([x[1] for x in sorted(group)])}\t{int(group_size)}")

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python group_accessions.py <path_to_tsv_file>")
    else:
        process_accessions(sys.argv[1])
