#!/bin/bash

#https://stackoverflow.com/questions/5914513/shuffling-lines-of-a-file-with-a-fixed-seed
get_seeded_random()
{
  seed="$1"
  openssl enc -aes-256-ctr -pass pass:"$seed" -nosalt \
    </dev/zero 2>/dev/null
}

zstdcat ../Athena_Dec_10_public.csv.zst  | awk -F, '{gsub(/"/, "", $1); gsub(/"/, "", $2); print $1"\t"$2}' |grep -v mbases | shuf --random-source=<(get_seeded_random 42)  | head -n 500000 > 181223-500k.tsv
