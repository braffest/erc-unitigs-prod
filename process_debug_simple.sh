subset=$(cat subset)
bucket=logan-dec2023-testbucket

date=$(date +"%b%d-%Y")
arch=$(uname -m)
tag=cuttlefisha_sra-$arch-$date-subset$subset

# do the subset one by one
#while IFS=$'\t' read -r accession size; do
#	python batch/simple_submit_job.py $accession $size $tag
#done < <(grep -vE "^(Run|acc)" subsets/subset_$subset.tsv)
#
python group_accessions.py subsets/subset_$subset.tsv | while IFS=$'\t' read -r accessions size; do
    python batch/simple_submit_job.py "$accessions" "$size" "$tag" "$bucket"
done
